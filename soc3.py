#!/usr/bin/env python3

import sys, os, socket, select, random, string, signal, subprocess

from time import sleep
from pprint import pprint

class Peer:
    """A Peer object listens and reply to requests it receives.
     It can also send requests to another peer and print the response it gets."""
     
    def signal_handler(self, signum, frame):
        """Exit process on signals SIGINT and SIGTERM."""
        print('PID/Frame '+str(os.getpid())+'/'+str(frame)+' received signal '+str(signum)+'', file=sys.stderr, flush=True)

        if signum in [2, 15]:
  
            sys.exit(0)
            
    def __init__(self):
        """Give a name to this peer and set it a lock file."""
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        
        self.name = ''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(8)).upper()
        self.lockfile = '/var/lock/'+sys.argv[0]+'.lock'

    def read_socket(self, socket, chunk_size = 32):
        """Read from socket until there is no more data. Return the data as a UTF-8 string."""
        l_data = []
        print('Reading '+self.str_address(socket.getsockname())+' (remote='+self.str_address(socket.getpeername())+')', file=sys.stderr, flush=True)
        while True:
        
            chunk = socket.recv(chunk_size)
            l_data.append(chunk)
            #~ print(chunk)
            if len(chunk) < chunk_size:
                
                b_data = b''.join(l_data)
                data = b_data.decode('UTF-8')
                return(data)
                
            else:
                
                continue

    def reply(self, message):
        """Reply to a message. '<address>:<port>' is the first field of the message."""
        l_message = message.split(' ')
        s_addr = l_message[0]
        s_host = s_addr.split(':')[0]
        i_port = int(s_addr.split(':')[1])
        
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((s_host, i_port))
        client.send(bytes(message, 'utf8'))
        client.close()

    def wait_socket_reply(self, socket):
        """Wait socket forever and reply when socket is ready."""
        while True:

            ready = select.select([socket],[],[])
            
            for conn in ready[0]:
            
                sock, addr = conn.accept()
                print(self.name+' accepted connection on '+self.str_address(sock.getsockname())+' from '+self.str_address(sock.getpeername()), file=sys.stderr, flush=True)
                
                request = self.read_socket(sock, 64)
                self.reply(request)

    def wait_socket_print(self, socket):
        """Wait socket, when ready: print the request body and return."""
        while True:

            ready = select.select([socket],[],[])
            
            for conn in ready[0]:
            
                sock, addr = conn.accept()
                print(self.name+' accepted connection on '+self.str_address(sock.getsockname())+' from '+self.str_address(sock.getpeername()), file=sys.stderr, flush=True)
                
                request = self.read_socket(sock, 64)
                request_body = ' '.join(request.split(' ')[1:])
                print(request_body)
                sock.close()
                return(True)

    def listen(self, address = '127.0.0.1', port = 5959):
        """Start a server to respond to received messages."""
        try:
            
            s_pid = str(os.getpid())
            print('\nStarting '+sys.argv[0]+' server at '+address+':'+str(port), file=sys.stderr, flush=True)
            
            self.listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.listener.setblocking(0)
            self.listener.bind((address, port))
            self.listener.listen()
            self.lock = open(self.lockfile, 'a')
            t_socket = self.listener.getsockname()
            s_addr = t_socket[0]
            s_port = str(t_socket[1])
            self.lock.write(self.name+' '+s_pid+' '+s_addr+':'+s_port+'\n')
            self.lock.close()
            
            print(sys.argv[0]+' server started. PID: '+s_pid+', Name: '+self.name, file=sys.stderr, flush=True)
            
            self.wait_socket_reply(self.listener)
       
        except OSError as e:
            
            # Address already in use
            if e.errno == 98:
                
                print(str(e), file=sys.stderr, flush=True)
            
            print('Cannot start !', file=sys.stderr, flush=True)
                
    def start(self):
        """Fork a server sub-process."""
        self.process = subprocess.Popen([sys.argv[0], 'listen'], stdin=subprocess.PIPE, shell=False)

    def stop(self):
        """Stop all running servers."""
        print('Stopping '+sys.argv[0]+' server(s)', file=sys.stderr, flush=True)
        
        try:
            
            self.lock = open(self.lockfile, 'r')
            locks = self.lock.read().split('\n')
            self.lock.close()
            
            for lock in locks:
                
                if lock:
                    
                    l = lock.split(' ')
                    print('Stopping '+l[0]+' (PID: '+l[1]+')', file=sys.stderr, flush=True)
                    
                    try:
                    
                        os.kill(int(l[1]), 15)
                        
                    except ProcessLookupError:
                        
                        print('No process with PID '+l[1]+' is running. (Name: '+l[0]+')', file=sys.stderr, flush=True)

            os.remove(self.lockfile)

        except FileNotFoundError:
            
            print('No lock file found.', file=sys.stderr)

    def servers(self):
        """Return the running server list from reading the lock file."""
        self.lock = open(self.lockfile, 'r')
        servers = '\n'.join([s for s in self.lock.read().split('\n') if s.strip()])
        self.lock.close()

        return(servers)

    def str_address(self, tuple_addr):
        """Return a string from a INET socket tuple."""
        return(tuple_addr[0]+':'+str(tuple_addr[1]))

    def address(self, name):
        """Return the ('host', port) tuple from a peer name (reading the lock file)."""
        self.lock = open(self.lockfile, 'r')
        servers = [s for s in self.lock.read().split('\n') if s.strip()]
        self.lock.close()
        
        for s in servers:
            
            _name = s.split(' ')[0]
            
            if name == _name:
                
                s_addr = s.split(' ')[2]
                l_addr = s_addr.split(':')
                i_port = int(l_addr[1])
                t_addr = (l_addr[0], i_port)
                
                return(t_addr)
        
    def send(self, peer_name, message):
        """Send a message to a given peer."""
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(self.address(peer_name))
        client.send(bytes(message, 'utf8'))
        client.close()

    def receiver(self):
        """Create a socket on local host. Return (socket, '<host>:<port>')."""
        answer_to = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        answer_to.setblocking(0)
        host = socket.gethostbyname(socket.gethostname()) 
        answer_to.bind((host, 5960))
        answer_addr = answer_to.getsockname()
        answer_str_addr = self.str_address(answer_addr)
        
        return(answer_to, answer_str_addr)

if __name__ == '__main__':
    
    peer = Peer()    

    try:

        if sys.argv[1] == 'start':

            peer.start()
            
        if sys.argv[1] == 'listen':
        
            peer.listen()

        if sys.argv[1] == 'stop':
        
            peer.stop()

        if sys.argv[1] == 'list':
        
            print(peer.servers())

        if sys.argv[1] == 'send':
        
            receiver = peer.receiver()
            receiver[0].listen()
            peer.send(sys.argv[2], receiver[1]+' '+' '.join(sys.argv[3:]))
            peer.wait_socket_print(receiver[0])


    except IndexError:
        
        print('Invalid command line', file=sys.stderr)    
