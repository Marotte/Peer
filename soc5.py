#!/usr/bin/env python3

import sys, os, socket, select, random, string, signal, subprocess

from multiprocessing import Queue, Process
from time import sleep

def str_address(tuple_addr):
    """Return a string from a INET socket tuple."""

    return(tuple_addr[0]+':'+str(tuple_addr[1]))

class Message:

    def __init__(self, headers, body = ''):
    
        self.headers = '\0'.join(map(str, headers))
        self.body = body
    
    def message(self):
        
        return(self.headers+'\n'+self.body)    
    
    def __repr__(self):
        
        return(self.headers.replace('\0','\t')+'\n'+self.body)

class Peer:

    server_port = 6969
    lockfile = '/var/lock/'+sys.argv[0]+'.lock'
    
    def signal_handler(self, signum, frame):
        """Exit process on signals SIGINT and SIGTERM."""
        print('PID/Frame '+str(os.getpid())+'/'+str(frame)+' received signal '+str(signum)+'', file=sys.stderr, flush=True)

        if signum in [2, 15]:

            sys.exit(0)
        
    def __init__(self, listen_address = ''):

        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        
        self.name = ''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(8)).upper()
        self.listen_address = listen_address

        self.request_queue = Queue()

    def read_socket(self, socket, chunk_size = 32):
        """Read from socket until there is no more data. Return the data as a UTF-8 string."""
        l_data = []
        print('Reading '+str_address(socket.getsockname())+' (remote='+str_address(socket.getpeername())+')', file=sys.stderr, flush=True)
        while True:
        
            chunk = socket.recv(chunk_size)
            l_data.append(chunk)

            if len(chunk) < chunk_size:
                
                b_data = b''.join(l_data)
                data = b_data.decode('UTF-8')
                return(data)
                
            else:
                
                continue

    def wait_socket_reply(self, socket):
        """Wait socket forever and reply when socket is ready."""
        while True:

            ready = select.select([socket],[],[])
            
            for conn in ready[0]:
            
                sock, addr = conn.accept()
                print(self.name+' accepted connection on '+str_address(sock.getsockname())+' from '+str_address(sock.getpeername()), file=sys.stderr, flush=True)

                request = self.read_socket(sock, 64)
                print('Request:\n'+request, file=sys.stderr)
                sock_name = sock.getsockname()
                peer_name = sock.getpeername()
                
                answer_headers = [sock_name[0], str(sock_name[1])]
                answer_body = request.split('\n',1)[1]
                message = Message(answer_headers, answer_body)
                print('Answer:\n'+message.message(), file=sys.stderr)
                self.send(peer_name[0], [message.body])
                sleep(3)

    def queue_request(self, request):
        
        self.request_queue.put(request)

    def wait_socket_queue(self, socket):
        """Start a process to work on the queue, then socket, when ready: enqueue the request."""

        while True:

            ready = select.select([socket],[],[])
            
            for conn in ready[0]:
            
                sock, addr = conn.accept()
                print(self.name+' accepted connection on '+str_address(sock.getsockname())+' from '+str_address(sock.getpeername()), file=sys.stderr, flush=True)
                
                data = self.read_socket(sock, 64)
                
                Process(target=self.queue_request, args=(data,)).start()
                Process(target=self.reply_request_queue()).start()

    def reply_request_queue(self):

        request = self.request_queue.get()
        _addr = request.split('\n',1)[0].split('\0')[0]
        _port = request.split('\n',1)[0].split('\0')[1]
        
        answer_headers = [_addr, str(_port)]
        answer_body = request.split('\n',1)[1]
        message = Message(answer_headers, answer_body)

        print('Replying request:\n'+request+'\nTo: '+_addr)

        self.send(_addr, [message.body])
        
        sleep(3)

        

                
    def wait_socket_print(self, socket):
        """Wait socket, when ready: print the request body and return."""
        while True:

            ready = select.select([socket],[],[])
            
            for conn in ready[0]:
            
                sock, addr = conn.accept()
                print(self.name+' accepted connection on '+str_address(sock.getsockname())+' from '+str_address(sock.getpeername()), file=sys.stderr, flush=True)
                
                data = self.read_socket(sock, 64)
                print(data, flush=True)
                return(True)
    
    def listen(self, address):
        
        try:
            
            s_pid = str(os.getpid())
            self.listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.listener.setblocking(0)
            
            self.listener.bind((address, self.server_port))
            self.listener.listen()

            print('\nStarting '+sys.argv[0]+' server at '+address+':'+str(self.server_port), file=sys.stderr, flush=True)
            self.lock = open(self.lockfile, 'a')
            t_socket = self.listener.getsockname()
            s_addr = t_socket[0]
            s_port = str(t_socket[1])
            self.lock.write(self.name+' '+s_pid+' '+s_addr+':'+s_port+'\n')
            self.lock.close()
        
            print(sys.argv[0]+' server started. PID: '+s_pid+', Name: '+self.name+', Listening on: '+str_address(t_socket), file=sys.stderr, flush=True)
        
            self.wait_socket_queue(self.listener)

        except OSError as e:
            
            # Address already in use
            if e.errno == 98:
                
                print(str(e), file=sys.stderr, flush=True)
                sys.exit(1)
                
            # Invalid argument
            if e.errno == 22:
                
                print(str(e), file=sys.stderr, flush=True)
                sys.exit(1)

    def start(self):
        """Fork a server sub-process."""
        self.process = subprocess.Popen([sys.argv[0], 'listen', self.listen_address], stdin=subprocess.PIPE, shell=False)

    def send(self, addr, l_data):
        
        try:
        
            client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client.connect((addr, 6969))
            client.setblocking(0)
            _mess = Message(client.getsockname(), ' '.join(l_data))
            client.send(bytes(_mess.message(), 'utf8'))
            client.close()

        except ConnectionRefusedError as e:
            
            print(str(e), file=sys.stderr)

class PeerManager:
    
    def __init__(self):
    
        self.lockfile = '/var/lock/'+sys.argv[0]+'.lock'
    
    def peers(self):
        """Return peers name list."""
        
        peers = []
        
        try:
        
            self.lock = open(self.lockfile, 'r')
            locks = self.lock.read().split('\n')
            self.lock.close()
        
            for lock in locks:
                
                l = lock.split(' ')
                peers.append(l[0])
            
        except FileNotFoundError as e:
            
            pass
            
        return(peers)    
        
    def stop_peer(self, peer):
        
        try:
            
            self.lock = open(self.lockfile, 'r')
            locks = self.lock.read().split('\n')
            self.lock.close()
            
            l_keep  = []
            
            for lock in locks:
                
                l = lock.split(' ')
                
                if l[0] == peer:

                    print('Stopping '+l[0]+' (PID: '+l[1]+')', file=sys.stderr, flush=True)
                    
                    try:
                    
                        os.kill(int(l[1]), 15)
                        
                    except ProcessLookupError:
                        
                        print('No process with PID '+l[1]+' is running. (Name: '+l[0]+')', file=sys.stderr, flush=True)

                else:
                    
                    l_keep.append(lock)

            s_keep = '\n'.join(l_keep)

            self.lock = open(self.lockfile, 'w')
            self.lock.write(s_keep)
            self.lock.close()

        except FileNotFoundError:
            
            print('No lock file found.', file=sys.stderr)

    def stop(self, peers):
        
        for peer in [p for p in peers if p is not '']:

            self.stop_peer(peer)


if __name__ == '__main__':

    try:
    
        manager = PeerManager()
        peer = Peer(socket.gethostbyname(socket.gethostname()))

        if sys.argv[1] == 'start':

            peer.start()

        elif sys.argv[1] == 'listen' and len(sys.argv) > 2:

            peer.listen(sys.argv[2])

        elif sys.argv[1] == 'stop' and sys.argv[2] == 'all':

            manager.stop(manager.peers())

        elif sys.argv[1] == 'stop' and len(sys.argv) > 2:

            manager.stop(sys.argv[2:])

        elif sys.argv[1] == 'send' and len(sys.argv) > 3:
            
            peer.send(sys.argv[2], sys.argv[3:])

    except IndexError as e:
        
        print(str(e), file=sys.stderr)
    
    
