#!/usr/bin/env python3

import sys, os, socket, select, random, string, signal, subprocess

from time import sleep
from pprint import pprint

def str_address(tuple_addr):
    """Return a string from a INET socket tuple."""
    return(tuple_addr[0]+':'+str(tuple_addr[1]))

class Message:
    
    def __init__(self, t_addr, body):
        
        self.from_address = t_addr
        self.headers = str_address(t_addr)
        self.body    = body

    def message(self):
        
        return(self.headers+'\n'+self.body)

    def getBody(self):
        
        return(self.body)

    def getHeaders(self):
        
        return(self.headers)
        
    def sender_addr(self):
        
        return(self.from_address)

class Peer:
    """A Peer object listens and reply to requests it receives.
     It can also send requests to another peer and print the response it gets."""
     
    def signal_handler(self, signum, frame):
        """Exit process on signals SIGINT and SIGTERM."""
        print('PID/Frame '+str(os.getpid())+'/'+str(frame)+' received signal '+str(signum)+'', file=sys.stderr, flush=True)

        if signum in [2, 15]:
  
            sys.exit(0)
            
    def __init__(self, listen_address = ''):
        """Give a name to this peer and set it a lock file."""
        signal.signal(signal.SIGTERM, self.signal_handler)
        signal.signal(signal.SIGINT, self.signal_handler)
        
        self.name = ''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(8)).upper()
        self.lockfile = '/var/lock/'+sys.argv[0]+'.lock'
        self.listen_address = listen_address

    def read_socket(self, socket, chunk_size = 32):
        """Read from socket until there is no more data. Return the data as a UTF-8 string."""
        l_data = []
        print('Reading '+str_address(socket.getsockname())+' (remote='+str_address(socket.getpeername())+')', file=sys.stderr, flush=True)
        while True:
        
            chunk = socket.recv(chunk_size)
            l_data.append(chunk)

            if len(chunk) < chunk_size:
                
                b_data = b''.join(l_data)
                data = b_data.decode('UTF-8')
                return(data)
                
            else:
                
                continue

    def reply(self, message):
        """Reply to a message. '<address>:<port>' is the first field of the message."""
        
        recipient_host = message.sender_addr()[0]
        print('Reply to '+recipient_host)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((recipient_host, 6960))
        client.send(bytes(message.getHeaders()+'\n'+message.getBody(), 'utf8'))
        
        client.close()

    def wait_socket_reply(self, socket):
        """Wait socket forever and reply when socket is ready."""
        while True:

            ready = select.select([socket],[],[])
            
            for conn in ready[0]:
            
                sock, addr = conn.accept()
                print(self.name+' accepted connection on '+str_address(sock.getsockname())+' from '+str_address(sock.getpeername()), file=sys.stderr, flush=True)

                request = self.read_socket(sock, 64)

                message = Message(sock.getpeername(), '\n'+''.join(request.split('\n',1)[1:]))
                print(message.getBody())
                self.reply(message)

    def wait_socket_print(self, socket):
        """Wait socket, when ready: print the request body and return."""
        while True:

            ready = select.select([socket],[],[])
            
            for conn in ready[0]:
            
                sock, addr = conn.accept()
                print(self.name+' accepted connection on '+str_address(sock.getsockname())+' from '+str_address(sock.getpeername()), file=sys.stderr, flush=True)
                
                request = self.read_socket(sock, 64)
                request_data = ''.join(request)
                request_body = ''.join(request_data.split('\n',1)[0:])
                print(request_body, flush=True)
                sock.close()
                return(True)

    def listen(self, host = '', port = 6959):
        """Start a server to respond to received messages."""
        try:
            
            s_pid = str(os.getpid())
            self.listener = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.listener.setblocking(0)
            
            if host == '':
                
                host = socket.gethostbyname(socket.gethostname())
            
            else:
                
                host = self.listen_address
            
            print('\nStarting '+sys.argv[0]+' server at '+host+':'+str(port), file=sys.stderr, flush=True)
            self.listener.bind((host, port))
            self.listener.listen()
            self.lock = open(self.lockfile, 'a')
            t_socket = self.listener.getsockname()
            s_addr = t_socket[0]
            s_port = str(t_socket[1])
            self.lock.write(self.name+' '+s_pid+' '+s_addr+':'+s_port+'\n')
            self.lock.close()
            
            print(sys.argv[0]+' server started. PID: '+s_pid+', Name: '+self.name+', Listening on: '+str_address(t_socket), file=sys.stderr, flush=True)
            
            self.wait_socket_reply(self.listener)
       
        except OSError as e:
            
            # Address already in use
            if e.errno == 98:
                
                print(str(e), file=sys.stderr, flush=True)
                print('Cannot start !', file=sys.stderr, flush=True)
                
    def start(self):
        """Fork a server sub-process."""
        self.process = subprocess.Popen([sys.argv[0], 'listen'], stdin=subprocess.PIPE, shell=False)

    def stop_all(self):
        """Stop all running servers."""
        print('Stopping '+sys.argv[0]+' server(s)', file=sys.stderr, flush=True)
        
        try:
            
            self.lock = open(self.lockfile, 'r')
            locks = self.lock.read().split('\n')
            self.lock.close()
            
            for lock in locks:
                
                if lock:
                    
                    l = lock.split(' ')
                    print('Stopping '+l[0]+' (PID: '+l[1]+')', file=sys.stderr, flush=True)
                    
                    try:
                    
                        os.kill(int(l[1]), 15)
                        
                    except ProcessLookupError:
                        
                        print('No process with PID '+l[1]+' is running. (Name: '+l[0]+')', file=sys.stderr, flush=True)

            os.remove(self.lockfile)

        except FileNotFoundError:
            
            print('No lock file found.', file=sys.stderr)

    def servers(self):
        """Return the running server list from reading the lock file."""
        self.lock = open(self.lockfile, 'r')
        servers = '\n'.join([s for s in self.lock.read().split('\n') if s.strip()])
        self.lock.close()

        return(servers)

    def address(self, name):
        """Return the ('host', port) tuple from a peer name (reading the lock file)."""
        
        try:
        
            self.lock = open(self.lockfile, 'r')
            
        except FileNotFoundError as e:
            
            print(str(e), file=sys.stderr)
        
        try:
        
            servers = [s for s in self.lock.read().split('\n') if s.strip()]
            self.lock.close()
        
            for s in servers:
                
                _name = s.split(' ')[0]
                
                if name == _name:
                    
                    s_addr = s.split(' ')[2]
                    l_addr = s_addr.split(':')
                    i_port = int(l_addr[1])
                    t_addr = (l_addr[0], i_port)
                    
                    return(t_addr)

        except AttributeError as e:
            
            print('No lock! Is the remote peer running? '+str(e), file=sys.stderr)
            return('',0)
        
    def send(self, peer_name, message):
        """Send a message to a given peer."""
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try: 
        
            client.connect(self.address(peer_name))
            client.send(bytes(message.getHeaders()+'\n'+message.getBody(), 'utf8'))
            client.close()
            
        except ConnectionRefusedError as e:
            
            print(str(e), file=sys.stderr)    
            return(False)

        return(True)

    def receiver(self):
        """Create a socket on local host. Return (socket)."""
        answer_to = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        answer_to.setblocking(0)
        host = socket.gethostbyname(socket.gethostname()) 
        answer_to.bind((host, 6960))

        return(answer_to)

    def sendto(self, peer_name, l_args):

        receiver = self.receiver()
        receiver.listen()
        mess = Message((receiver.getsockname()), ' '.join(l_args))
        print(mess.message())
        self.send(peer_name, Message((receiver.getsockname()), ' '.join(l_args)))
        self.wait_socket_print(receiver)
        receiver.close()

if __name__ == '__main__':
    
    peer1 = Peer('127.0.0.1')
    peer2 = Peer()


    try:

        if sys.argv[1] == 'start':

            peer1.start()
            
        if sys.argv[1] == 'listen':
        
            peer1.listen(peer1.listen_address)

        if sys.argv[1] == 'stopall':
        
            peer1.stop_all()

        if sys.argv[1] == 'list':
        
            print(peer1.servers())

        if sys.argv[1] == 'send':
        
            peer2.sendto(sys.argv[2], sys.argv[3:])


    except IndexError:
        
        print('Invalid command line', file=sys.stderr)    
